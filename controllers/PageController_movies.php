<?php

class PageController_movies
{
	private static $_msg;
	public static function index()
	{
		$msg = self::$_msg;
		$paginator = new Paginator;
		if(!empty($_FILES['table_upload']))
		{
			$table = FileModel::fileCheck($_FILES['table_upload']);
			$page = $paginator->getURN();
			$paData = Database::getMovieData($page[4], $page[2]);
			$pagination = $paginator->createLinks();
			echo View::Template('movies', array(
									'data' 		=> $paData,
									'msg' 		=> $table,
									'page'		=> $page,
									'paginator'	=> $paginator,
									'pagination'=> $pagination
									));
		}
		else
		{
			if(isset($_POST['paginateInput']) && !empty($_POST['paginateInput']))
			{
				$page = $paginator->getURN();
				$page[4] = $_POST['paginateInput'];
			}
			else
			{ 
				if(isset($_POST['paginatedDrop']) && !empty($_POST['paginatedDrop']))
				{
					$page = $paginator->getURN();
					$page[4] = (int)$_POST['paginatedDrop'];
				}
				else
				{
					$page = $paginator->getURN();
				}
			}
			$paData = Database::getMovieData($page[4], $page[2]);
			$total = Database::getTotalMovies();
			$pagination = $paginator->createLinks($total,$page[4],$page[2] , '/movies/index/' );
			$links = $paginator->paginatedLinks($total,$page[4],$page[2]);

			echo View::Template('movies', array(
									'msg' 		=> $msg,
									'data' 		=> $paData,
									'paged' 	=> $page,
									'paginator'	=> $paginator,
									'pagination'=> $pagination,
									'links' => $links
									));
		}
	
	}
	public static function show()
	{
		$id = (int)Router::$URNParts[2];
		$total = Database::getTotalMovies();
		if($id > $total)
		{
			$msg = "This is not the result you've been looking for!";
			echo View::template('moviedata', array('msg' => $msg));
		}
		else
		{
			$row = Database::getMovie($id);
			echo View::template('moviedata', array('data' => $row));
		}
	}
}
