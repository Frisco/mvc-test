<?php 

class ImgResize
{
	public static function resizeImage($imgFile, $width, $height)
	{
		if($width > 1200 && $height > 1200)
		{
			$width = 1200;
			$height = 1200;
		}
		elseif($height > 1200)
		{
			$height = 1200;
		}
		elseif($width > 1200)
		{
			$width = 1200;
		}
		$img = new Imagick('uploads/'.$imgFile);
		$img->cropThumbnailImage($width, $height);
		if(!file_exists('resimages/'.round($width).'x'.round($height).DIRECTORY_SEPARATOR))
		{
			mkdir('resimages/'.round($width).'x'.round($height).DIRECTORY_SEPARATOR, 0777, true);
			$status = $img->writeImage('resimages/'.round($width).'x'.round($height).DIRECTORY_SEPARATOR.$imgFile);
		}
		elseif(file_exists('resimages/'.round($width).'x'.round($height).DIRECTORY_SEPARATOR))
		{
			$status = false;
		}
		return $status;
	}
}