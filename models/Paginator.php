<?php

class Paginator
{

    public function __construct()
    {

    }
	public static function getURN()
	{
		$URNParts = Router::$URNParts;
		$count = count($URNParts);
		if($count < 3)
		{
			$URNParts = array($URNParts, '/index/', 1, '/limit/', 25);
		}
		else
		{
			$URNParts;
		}
		return $URNParts;
	}

	public function createLinks($total,$limit,$currentPage, $uri) 
    {	
    	$this->total = $total;
    	
    	$this->limit = $limit;
    	
    	$this->page = $currentPage;
        $boxes = 4;
        $first = 1;
        $perPage = 10;
        $lim = "/limit/";
        
        $links = array();
        $lastPage = ceil($total / $limit);
        
        if ($currentPage != 1)
        {
			$links[] = array(
						'name' 	=> '<',
						'link'	=> $uri . '1' .$lim .$limit
						);
		}
		
        for ($i=($currentPage - ceil($perPage / 2)); $i<($currentPage + ceil($perPage / 2)); $i++)
        {
				if ($i<1 || $i > $lastPage+1) continue;
				$links[] = array(
						'name' 	=> intval($i),
						'link'	=> $uri . $i .$lim .$limit
						);
		}
		
        if ($currentPage != $lastPage)
        {
			$links[] = array(
						'name' 	=> '>',
						'link'	=> $uri . ceil($lastPage) .$lim .$limit
						);
        }
      return $links;
    }

    public function paginatedLinks($total,$limit,$currentPage)
    {
        $page = $currentPage;
        $boxes = 4;
        $first = 1;
        $uri = '/movies/index/';
        $limitstring = '/limit/';
        $last = ceil( $total / $limit );
        $start = (( $page - $boxes ) > 0 ) ? $page - $boxes : 1;
        $end = (( $page + $boxes ) < $last ) ? $page + $boxes : $last;

        $links = array(
                'uri'           => $uri,
                'first'         => 1,
                'total'         => $total,
                'limit'         => $limitstring.$limit,
                'page'          => $page,
                'last'          => $last,
                'start'         => $start,
                'end'           => $end,
                'firstLink'     => $uri.$first.$limitstring.$limit,
                'startLink'     => $uri.$start.$limitstring.$limit,  
                'currentLink'   => $uri.$page.$limitstring.$limit,
                'endLink'       => $uri.$end.$limitstring.$limit,
                'lastLink'      => $uri.$last.$limitstring.$limit,
                'prevLink'      => $uri.($page - 1).$limitstring.$limit,
                'nextLink'      => $uri.($page + 1).$limitstring.$limit,
                );
        return $links;
    }
}
