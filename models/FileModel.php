<?php

class FileModel
{
	private static $_instance = null;
	static $msgCode = array(
					1 => 'The uploaded file exceeds the 2MB limit!',
					2 => 'Plase select a file to upload',
					3 => 'The file is not in the proper extension. It has to be: .jpg, .png, .jpeg, .gif and/or .csv');

	public function __construct()
	{
		
	}
	public static function getInstance()
	{
		if (self::$_instance == null)
			self::$_instance = new FileUpload();
		return self::$_instance;
	}

	public static function fileCheck($fileData)
	{
		$error = $fileData['error'];
		if($error == 4)
		{
			$msg = self::$msgCode[2];
		}
		else
		{
			$status = true;
			$dir = "uploads/";
			$fileToUpload = $fileData['tmp_name'];
			$targetFile = $dir . basename($fileData['name']);
			$fileType = pathinfo($targetFile, PATHINFO_EXTENSION);
			$msg = '';
			if($fileData['size']  > 2000000)
			{
				$msg .= self::$msgCode[1].'<br>';
				$status = false;
			}
			if($fileType != 'jpg' && $fileType != 'jpeg' && $fileType != 'png' && $fileType != 'gif' && $fileType != 'csv')
			{
				$msg .= self::$msgCode[3];
				$status = false;
			}
			if($error === 0 && $status == true)
			{
				if($fileType === 'csv')
				{
					$handle = fopen($fileToUpload, "r");		  		
					while(!feof($handle))
					{
						$csvData[] = self::customGetCSV($handle, 1400, ";");
       				} 
       				fclose($handle);
       				$msg = Database::insertIntoProgram($csvData);
				}
				else
				{
					if(file_exists($targetFile))
					{
						$msg = "File already exists.";
					}
					else
					{
						move_uploaded_file($fileToUpload, $targetFile);
						$msg = "There was no error, the file uploaded with success";
					}
				}
			}
		}
		return $msg;
	}
	public static function removeFile($file)
	{
		if(unlink('uploads'. DIRECTORY_SEPARATOR . $file))
		{
			return $msg = "Damage was done! It was deleted!";
		}
		else
		{
			return $msg = "Nope! There is no spoon ... file!";
		}
	}

	public static function delDir($dir)
	{
		$dir = 'resimages'.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR;
		$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file)
		{
			if($file->isDir())
			{
				rmdir($file->getRealPath());
			}
			else
			{
				unlink($file->getRealPath());
			}
		}
		$msg = rmdir($dir);
		return $msg;
	}

	public static function customGetCSV($handle, $length, $separator = ';')
	{
    	if (($buffer = fgets($handle, $length)) !== false) 
    	{
        	return explode($separator, iconv("CP1251", "UTF-8", $buffer));
    	}
    	return false;
	}
	public static function getFilesFromDir($dir)
	{
		$files = scandir(''.$dir.'/');
		unset($files[0], $files[1]);
		return $files;
	}
}