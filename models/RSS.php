<?php

class RSS
{
	private static $instance = null;
	private function __construct()
	{

	}

	public static function getInstance()
	{
		if (self::$instance === null)
		{
			self::$instance = new RSS();
		}
		return self::$instance;
	}
	public static function getAllData()
	{
		$dbc = Database::getInstance();
		$sql = "SELECT `program`.*, `cities`.city_name FROM `program` JOIN `cities`
				WHERE `program`.city_id = `cities`.city_id";
		$stmt = $dbc->prepare($sql);
		$dbc->exec("SET NAMES 'UTF8'");
		$stmt->execute();
		$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $res;
	}
	public static function getMoviesBy($table, $value)
	{
		$dbc = Database::getInstance();
		if($table == 'city')
		{
			$sql = "SELECT `program`.*, `cities`.city_name FROM `program` JOIN `cities`	WHERE `cities`.city_name = ? AND `program`.city_id = `cities`.city_id";
			$dbc->exec("SET NAMES 'UTF8'");
			$stmt = $dbc->prepare($sql);
			$stmt->bindParam(1, $value);
		}
		else
		{
			$sql = "SELECT `program`.*, `cities`.city_name FROM `program` JOIN `cities`	WHERE `program`.city_id = `cities`.city_id AND (`program`.".$table." LIKE ? )";
			$dbc->exec("SET NAMES 'UTF8'");
			$stmt = $dbc->prepare($sql);
			$stmt->bindValue(1, '%'.$value.'%');

		}
		$stmt->execute();
		$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $res;
	}
}