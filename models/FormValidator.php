<?php

/**
 * The FormValidator
 */

class FormValidator
{

	private static $currentFormData;
	private static $errors = array();

	public static function check($formData, $validatorRules)
	{
		self::$errors = array();

		$formData = self::sanitizeInput($formData);
		self::$currentFormData = $formData;
		echo '<pre>'.print_r($formData, true).'</pre>';
		foreach($formData as $formDataKey => $formDataValue)
		{	
			if(isset($validatorRules[$formDataKey]))
			{	
				$status = true;
				$rules = explode('|', $validatorRules[$formDataKey]);
				foreach ($rules as $rule)
				{
					$ruleElement = explode(":",$rule);
					$method = $ruleElement[0];
					unset($ruleElement[0]);
					$ruleElement = array_values($ruleElement);
					$result = self::$method($formDataKey, $formDataValue, $ruleElement);
					$status = $status && $result;
				}
			}
		}

	}
	/**
	 * Филтрира данните от input подадени от контролера	
	 * @param  array $formData 
	 * @return array           
	 */
	private static function sanitizeInput($formData)
	{
		$sanitizedData = filter_var_array($formData, FILTER_SANITIZE_STRING);
		return $sanitizedData;
	}
	/**
	 * Rules / Правила - извършващи се от валидатора, като всеки един метод
	 * приема определена стойност от масива $formData на база на модела:
	 * ValidatorRules
	 */
	
	/**
	 * Проверява дали даденото input поле отговаря на правилото за минимална
	 * стойност зададено в метода ValidatorRules
	 * @param  string $value        
	 * @param  array  $validateData 
	 * @return boolean
	 */
	private static function minLenght($key, $value, array $validateData)
	{	
		if (strlen($value) > $validateData[0])
			return true;
		else
		{
			self::$errors[$key]['minLenght'] = "This field must be atleas ".$validateData[0]." chars long!";
			return false;
		}
	}

	/**
	 * Проверява дали даденото input поле отговаря на правилото за максимална
	 * стойност зададено в метода ValidatorRules
	 * @param  string $value        
	 * @param  array  $validateData 
	 * @return boolean
	 */
	private static function maxLenght($key, $value, array $validateData)
	{	
		if (strlen($value) < $validateData[0])
			return true;
		else
		{
			self::$errors[$key]['maxLenght'] = "This field must be less than ".$validateData[0]." chars long!";
			return false;
		}
	}

	/**
	 * Проверява дали даденото input поле е с цифрови стойности
	 * @param  string $value        
	 * @param  array  $validateData 
	 * @return boolean               
	 */
	private static function numeric($key, $value, array $validateData)
	{
		$status = is_numeric($value);
		if($status == false)
		{
			self::$errors[$key]['numeric'] = 'This field must contain only numeric symbols!';
		}
		return $status;
	}

	/**
	 * Проверява дали в даденото input поле думата започва с главна буква, а 
	 * останалите са с малка.
	 * @param  string $value
	 * @return boolean        
	 */
	private static function firstUpper($key, $value)
	{	
		$comparison = ucwords(strtolower($value));
		if($comparison === $value)
			return true;
		else
		{
			self::$errors[$key]['firstUpper'] = 'This field must have: first letter capital, others to be lower case!';
			return false;
		}
	}

	/**
	 * Проверява дали даденото input поле е във формат за email.
	 * @param  string $value
	 * @return boolean        
	 */
	private static function email($key, $value)
	{
		if(filter_var($value, FILTER_VALIDATE_EMAIL))
			return true;
		else
		{
			self::$errors[$key]['email'] = 'This is an email field, it must contain email formatting: example@domain.com';
			return false;
		}
	}

	/**
	 * Проверява дали даденото input поле съдържа само букви и цифри.
	 * @param  string $value
	 * @return boolean        
	 */
	private static function alphaNumeric($key, $value)
	{
		$result = ctype_alnum($value);
		if($result == false)
		{
			self::$errors[$key]['alphaNumeric'] = 'This field must contain only letters and numbers!';
		}
		return $result;
	}

	/**
	 * Проверява дали даденото input поле съдържа само букви
	 * @param  string $value
	 * @return boolean        
	 */
	private static function alpha($key, $value)
	{
		$result = ctype_alpha($value);
		if($result == false)
		{
			self::$errors[$key]['alpha'] = 'This field must contain only alphabetical characters';
		}
		return $result;
	}

	/**
	 * Проверка за полето парола, дали отговаря на следните изисквания:
	 * 1. Да има поне една главна буква;
	 * 2. Да има поне една малка буква;
	 * 3. Да има поне една цифра;
	 * 4. Да има поне един специален символ;
	 * @param  string $value
	 * @return boolean        
	 */
	private static function password($key, $value)
	{
		if(!preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]$/', $value))
		{
			self::$errors[$key]['password'] = 'The '.$key.'field must contain: Atleas one capital letter, atleast one lowercase letter, atleast one number, and atleast one special symbol';
			return false;
		}
		else
			return true;
	}
	/**
	 * Проверка дали полето е същото като това което е зададено след
	 * 	same:[fieldName]
	 * @param  string $value        
	 * @param  string $validateData 
	 * @return boolean               
	 */
	private static function same($key, $value, $validateData)
	{
		var_dump(self::$currentFormData[$validateData]);
		var_dump($value);
		exit();
		if($value === self::$currentFormData[$validateData])
			return true;
		else
		{
			self::$errors[$key]['same'] = 'The field does not match the '.self::$currentFormData[$validateData].' value';
			return false;
		}
	}

	/**
	 * Проверява дали полето не е празно и ако е, връща грешка, че е 
	 * задължително	
	 * @param  string $value
	 * @return boolean
	 */
	private static function required($key, $value)
	{
		if(empty($value))
		{
			self::$errors[$key]['required'] = 'This field is required!';
			return false;
		}
		else
			return true;
	}

	private static function unique($value)
	{
		$dbc = Database::getInstance();
		$dbc->exec("SET NAMES 'UTF8'");
		$sql = "SELECT * FROM users WHERE email = :email";
		$stmt = $dbc->prepare($sql);
		$stmt->bindParam(':email', $value);
		$stmt->execute();
		$result = $stmt->fetchColumn();
		if(!$result)
			return false;
		else
			return true;
	}
	/**
	 * Returns the errors in an array
	 * @return array
	 */
	public static function getErrors()
	{
		return self::$errors;
	}
}