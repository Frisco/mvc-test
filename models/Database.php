<?php

class Database
{
	
	static $instance = null;
	public static $_limit;
    public static $_page;
    public static $_total;

	private function __construct()
	{

	}

	public static function getInstance()
	{
		if (self::$instance === null)
			self::createConnection();

		return self::$instance;
	}

	private static function createConnection()
	{
		$dbconn = 'mysql:host=localhost;dbname=cinema_program;';
		$user = 'root';
		$pass = 'probationer';
		
		self::$instance = new PDO($dbconn, $user, $pass);
	}

	public static function getTotalMovies()
	{
		$dbc = self::getInstance();
		$sql1 = "SELECT COUNT(*) FROM program";
		$stmt1 = $dbc->prepare($sql1);
		$stmt1->execute();
		$res1 = $stmt1->fetchAll();
		return  $res1[0][0];
	}
	public static function getProgram()
	{
		$dbc = self::getInstance();
		$sql = "SELECT `program`.*, `cities`.city_name FROM `program` JOIN `cities`
				WHERE `program`.city_id = `cities`.city_id";
		$stmt = $dbc->prepare($sql);
		$dbc->exec("SET NAMES 'UTF8'");
		$stmt->execute();
		$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $res;
	}

	public function getMovieData($limit, $page)
	{	
		$dbc = self::getInstance();
		self::$_limit = $limit;
		self::$_page = $page;
		if(self::$_limit === null)
		{
			self::$_limit = 25;
		}
		$sql1 = "SELECT COUNT(*) FROM program";
		$stmt1 = $dbc->prepare($sql1);
		$stmt1->execute();
		$res1 = $stmt1->fetchAll();
		self::$_total = $res1[0][0];
		$sql = "SELECT `program`.*, `cities`.city_name FROM `program` JOIN `cities`
				WHERE `program`.city_id = `cities`.city_id";
		$sql .= " LIMIT " . ((self::$_page - 1) * self::$_limit) . ", ".self::$_limit."";
		$stmt = $dbc->prepare($sql);
		$dbc->exec("SET NAMES 'UTF8'");
		$stmt->execute();
		$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $res;
	}

	public static function insertIntoProgram($data)
	{
		$dbc = self::getInstance();
		$sql1 = "SELECT city_id FROM cities WHERE city_name = :city_name";
		$sql2 = "INSERT IGNORE INTO program( cinema, movie, city_id, price, from_date, till_date, time, note ) VALUES (:cinema, :movie, :city_id, :price, :from_date, :till_date, :time, :note)";
		$dbc->exec("SET NAMES 'UTF8'");
		$stmt = $dbc->prepare($sql1);
		$stmt->bindParam(':city_name', $city_name);
		$stmt2 = $dbc->prepare($sql2);
		$stmt2->bindParam(':cinema', $cinema);
		$stmt2->bindParam(':movie', $movie);
		$stmt2->bindParam(':city_id', $city_id);
		$stmt2->bindParam(':price', $price);
		$stmt2->bindParam(':from_date', $from_date);
		$stmt2->bindParam(':till_date', $till_date);
		$stmt2->bindParam(':time', $time);
		$stmt2->bindParam(':note', $note);
		$DBData = self::getProgram();
		$comparedArray = array_intersect($data, $DBData);
		$num = count($comparedArray);
		for ( $i = 1; $i < $num ; $i++ )
		{
			if(array_key_exists($i, $comparedArray))
			{
				$city_name = $comparedArray[$i][0];
				$stmt->execute();
				$city_id = (int)$stmt->fetchColumn();
				$cinema = $comparedArray[$i][1];
				$movie = $comparedArray[$i][6];
				$price = $comparedArray[$i][3];
				$from_date = $comparedArray[$i][4];
				$till_date = $comparedArray[$i][5];
				$time = str_split($comparedArray[$i][2], 2);
				$time = implode(':', $time).':00';
				$note = $comparedArray[$i][7];
				$stmt2->execute();
			}
			
		}
		$sqlErr = $stmt2->errorInfo();
		if($sqlErr[0] == 0000)
		{
			$msg = 'Successfull!';
		}
		else
		{
			$msg = $sqlErr[2];
		}
		return $msg;
	}

	public static function getMovie($id)
	{
		$dbc = self::getInstance();
		$sql = "SELECT `program`.*, `cities`.city_name FROM `program` JOIN `cities`	WHERE `program`.program_id = :id AND `program`.city_id = `cities`.city_id";
		$stmt = $dbc->prepare($sql);
		$stmt->bindParam(':id', $id);
		$dbc->exec("SET NAMES 'UTF8'");
		$stmt->execute();
		$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $res;
	}

	public static function getCities()
	{
		$dbc = self::getInstance();
		$sql = "SELECT * FROM cities";
		$dbc->exec("SET NAMES 'UTF8'");
		$stmt = $dbc->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}
