<?php echo View::template('html' , array('title' => 'Image upload')); ?>
<div class="container">
	<?php
		if(!empty($msg))
		{
			echo '<div class="alert alert-warning">'.$msg.'</div>';
		}
	?>		
		<div class="well">
			<h2 class="text-center">Upload form</h2>
			<form method="post" enctype="multipart/form-data" action="#">
			<div class="text-center">
				<input class="btn btn-primary" style="margin: 0 auto;" type="file" name="file_upload">
				<input class="btn btn-info" style="margin-top: 5px;" type="submit">
			</div>
			</form>
		</div>
		<table class="table table-hover">
			 <thead>
			  <tr>
			   <th>Filename</th>
			   <th>Options</th>
			   <th>Operation</th>
			  </tr>
			 </thead>
			 <tbody>
			<?php
			if(is_array($uri) && empty($uri))
			{
				foreach($files as $file)
				{
					echo '<tr>
							<td>'.$file.'</td>
							<td><a class="btn btn-warning" href="/images/show/'.$file.'">Open Dir</a></td>
							<form method="post" action="">
							<input type="hidden" name="dirName" value='.$file.'>
							<td><button type="submit" class="btn btn-danger">Remove</button></td>
							</form>
						</tr>';
				}
			}
			else
			{
				foreach($files as $file)
				{
					echo '<tr>
							<td>'.$file['fileName'].'</td>
							<td><a href="/images/resize/'.$file['width'].'x'.$file['height'].'/'.$file['fileName'].'" class="btn btn-success">Options</a></td>
							<form method="post" action="">
							<input type="hidden" name="fileName" value='.$file['fileName'].'>
							<td><button type="submit" class="btn btn-danger">Remove</button></td>
							</form>
						  </tr>';
				}
			}
			
			?>
			 </tbody>
			</table>
	</div>