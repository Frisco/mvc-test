<?php
$html = '<ul class = "pagination pagination-sm">';
if($data['page'] == 1)
{
    $class = "disabled";
    $html .= '<li class="'.$class.'"><a>&laquo;</a></li>';
    $html .= '<li class="'.$class.'"><a>&lt;</a></li>';
}
else
{
    $class = "";
    $html .= '<li class="'.$class.'"><a href="'.$data['firstLink'].'">&laquo;</a></li>';
    $html .= '<li class="'.$class.'"><a href="'.$data['prevLink'].'">&lt;</a></li>';
}
if($data['start'] > 1 )
{
    $html .= '<li class="disabled"><a href="'.$data['firstLink'].'">1</a></li>';
    $html .= '<li class="disabled"><span>...</span></li>';
}
for ( $i = $data['start'] ; $i <= $data['end']; $i++ ) 
{
    $class = ( $data['page'] == $i ) ? "active" : "";
    $html .= '<li class="'.$class.'"><a href="'.$data['uri'].$i.$data['limit'].'">'.$i.'</a></li>';
}
if ( $data['end'] < $data['last'] ) 
    {
        $html .= '<li class="disabled"><span>...</span></li>';
        $html .= '<li class="disabled"><a href="'.$data['lastLink'].'">'.$data['last'].'</a></li>';
    }
if( $data['page'] == $data['last'] )
{
    $class = "disabled";
    $html .= '<li class="'.$class.'"><a>&gt;</a></li>';
    $html .= '<li class="'.$class.'"><a>&raquo;</a></li></ul>';
}
else
{
    $class = "";
    $html .= '<li class="'.$class.'"><a href="'.$data['nextLink'].'">&gt;</a></li>';
    $html .= '<li class="'.$class.'"><a href="'.$data['lastLink'].'">&raquo;</a></li>';
    $html .= '</ul>';
}
echo $html;