<?php echo View::template('html' , array('title' => 'Image upload')); ?>
<div class="container">
	<div class="row">
		<div class="col-md-8">
			<img style="width:100%; height=100%;" src="/resimages/<?php echo $img; ?>" alt="">
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
			<?php
			foreach ($files as $file)
			{
				if($file['fileName'] === $uri[3])
				{
					echo '<div class="panel-heading">
							'.$file['fileName'].'
							</div>
					<div class="panel-body">
						<p class="text-center">Real Size: </p>
						<p>Width: '.$file['width'].'</p>
						<p>Height: '.$file['height'].'</p>
						<p>Image path: <a class="btn btn-default" href="/images/show/'.$img.'">Image</a></p>
					</div>
					';
				} 
			}
			
			?>
			</div>
		</div>
	</div>
</div>
