<?php echo View::template('html' , array('title' => 'Image upload')); ?>

<div class="container">
	<?php
		if(!empty($msg))
		{
			echo '<div class="alert alert-warning text-center">'.$msg.'</div>';
		}
	?>	
	<div class="jumbotron">
		<div class="container">
			<h1 class="text-center">Resize!</h1>
			<p class="text-center">You can select following sizes: </p>
			<hr>
			
			<div class="col-md-4 text-center well">
			<p>320 x 240</p>
				<a href="/images/resize/320x240/<?php echo $img; ?>" class="btn btn-success">Select</a>
			</div>
			<div class="col-md-4 text-center well">
			<p>640 x 480</p>
				<a href="/images/resize/640x480/<?php echo $img; ?>" class="btn btn-warning">Select</a>
			</div>
			<div class="col-md-4 text-center well">
			<p>1024 x 768</p>
				<a href="/images/resize/1024x768/<?php echo $img; ?>" class="btn btn-info">Select</a>
			</div>
			<br>
			
			<h1 class="text-center">View Image!</h1>
			<hr>
			<div class="text-center">
				<a class="btn btn-lg btn-primary" href="/images/showImage/<?php echo $uri[2].'/'.$uri[3] ?>">View</a>
			</div>
		</div>
	</div>
</div>