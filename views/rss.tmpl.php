<?php header("Content-type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8"?>
	<rss version="2.0">
		<channel>
		 <title>Инвестор.БГ АД</title>
		 <link>http://ibg.bg/</link>
		 <description>Cinema Program for Bulgarian Theaters</description>
		 <language>bg</language>
		 <copyright>Copyright (C) 2009 investor.BG</copyright>';
		 foreach($data as $item) 
		 {
		 	echo "
		 	<item>
		 		<title>".$item['movie']."</title>
				<link>http://mvc.alex/movies/show/".$item['program_id']."</link>
				<description>
					<cinema>".$item['cinema']."</cinema>
					<time>".$item['time']."</time>
					<fromDate>".$item['from_date']."</fromDate>
					<untillDate>".$item['till_date']."</untillDate>
				</description>
				<category>Кино</category>".'
				<guid isPermaLink="true">http://mvc.alex/movies/show/'.$item['program_id'].'</guid>
				<pubDate>Tue, 05 June 2015 8:55:00 +0700</pubDate>
			</item>
					';
		 } 
echo 	'</channel>
	</rss>';